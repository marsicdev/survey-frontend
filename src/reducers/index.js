import { combineReducers } from 'redux'
import authReducer from '../app/auth/authReducer'

export default combineReducers({
    auth: authReducer
})
