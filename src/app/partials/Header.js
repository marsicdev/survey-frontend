import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Payment from '../payment/Payment'

class Header extends Component {

    renderContent = () => {
        const { auth } = this.props
        switch (auth) {
            case null:
                return
            case false:
                return <li><a href="/auth/google">Login with Google</a></li>
            default:
                return [
                    <li key="payment"><Payment /></li>,
                    <li key="credit">Credit: {auth.credits} </li>,
                    <li key="logout"><a href="/api/logout">Logout</a></li>
                ]
        }
    }

    render() {

        const { renderContent } = this
        const { auth } = this.props

        return (
            <header>
                <nav >
                    <div className="nav-wrapper">
                        <div className="container" >
                            <Link
                                to={auth ? '/surveys' : '/'}
                                className="left brand-logo"
                            >
                                Hey Survey
                            </Link>
                            <ul id="nav-mobile" className="right">
                                {renderContent()}
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    // return {auth: state.auth}
    return { auth }
}

export default connect(mapStateToProps)(Header)
