import React from 'react'

const Landing = () => {
    return (
        <div className="center">
            <h1>Hey Survey</h1>
            <p>Collect data from users</p>
        </div>
    )
}

export default Landing
