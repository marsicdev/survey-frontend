import { apiService } from '../../shared/APIService'

export const FETCH_USER = 'FETCH_USER'

export const fetchUser = () => async (dispatch) => {
    const user = await apiService.get('/api/currentUser')
    dispatch({ type: FETCH_USER, payload: user })
}
