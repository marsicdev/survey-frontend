import React, { Fragment } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import * as actions from './users/userAction'

import Header from './partials/Header'
import Landing from './partials/Landing'

const Dashboard = () => <h1>Dashboard</h1>

class App extends React.Component {

    componentDidMount() {
        this.props.fetchUser()
    }

    render() {
        return (
            <BrowserRouter>
                <Fragment>
                    <Header />
                    <main className="container">
                        <Route exact path="/" component={Landing} />
                        <Route path="/surveys" component={Dashboard} />
                    </main>
                </Fragment>
            </BrowserRouter>
        )
    }
}

export default connect(null, actions)(App)
