import { apiService } from '../../shared/APIService'
import { FETCH_USER } from '../users/userAction'

export const handlePayment = token => async dispatch => {
    const payment = await apiService.post('/api/payment', token)

    dispatch({ type: FETCH_USER, payload: payment })
}
