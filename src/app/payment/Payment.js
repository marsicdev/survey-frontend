import React, { Component } from 'react'
import StripeCheckout from 'react-stripe-checkout'
import { connect } from 'react-redux'
import * as actions from './paymentAction'

const moneyAmount = 500 // $ 5 = 500 cents

class Payment extends Component {

    render() {
        const { handlePayment } = this.props
        return (
            <StripeCheckout
                name="HeySurvey"
                description="$5 for 5 surveys"
                amount={moneyAmount}
                token={handlePayment}
                stripeKey={process.env.REACT_APP_STRIPE_KEY}
            >
                <button className="btn" >
                    Add credits
                </button>
            </StripeCheckout>
        )
    }
}

export default connect(null, actions)(Payment)
