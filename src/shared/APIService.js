import axios from 'axios'

export class APIService {

    get = (path) => axios.get(path)
        .then(res => res.data)

    post = (path, data) => {
        return axios.post(path, data)
            .then(res => res.data)
    }
}

export const apiService = new APIService()
